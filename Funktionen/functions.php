<?php

function helloWorld() {
	return "Hello World";
}

echo helloWorld();
// ==> Hello World

function say($msg = "Hello World") {
	echo $msg;
}

say("Hello World");
// ==> Hello World
say();
// ==> Hello World
?>
