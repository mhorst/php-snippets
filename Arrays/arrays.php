<?php
// Verschiedene Möglichkeiten
$meinArray = array();
$meinArray[] = "Erster Eintrag";
$meinArray[] = "zweiter Eintrag";

// löscht die Variable
unset($meinArray);

$meinArray = array();
$meinArray['Titl1'] = "Erster Eintrag";
$meinArray['Titl2'] = "Zweiter Eintrag";

unset($meinArray);

$meinArray = array("Erster Eintrag", "Zweiter Eintrag");

unset($meinArray);

$meinArray = array("Titel1" => "Erster Eintrag", "Titel2" => "Zweiter Eintrag");

unset($meinArray);

// Mehrdimensionale Arrays
// Statt eines Strings wird einfach ein Array an diese Stelle geschrieben

$meinArray = array();
$meinArray[0] = array();
$meinArray[0]["Titel1"] = "Erster Eintrag";
$meinArray[0]["Titel2"] = "Erster Eintrag";
$meinArray[1] = array();
$meinArray[1]["Titel1"] = "Erster Eintrag";
$meinArray[1]["Titel2"] = "Erster Eintrag";

unset($meinArray);

$meinArray = array("dasArray" => array("1", "2", "3"));
?>