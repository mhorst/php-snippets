<?php

// IF
$x = false;

if ($x == true) {
	echo "True";
} else {
	echo "False";
}

// Switch Case
$i = 5;

switch($i) {
	case '1' :
		echo "1 Gefunden!";
		break;

	case '5' :
		echo "5 Gefunden!";
		break;

	default :
		echo "Nicht gefunden!";
		break;
}
?>