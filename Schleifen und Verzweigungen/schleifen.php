<?php
$meinArray = array("1" => "a", "2" => "b", "3" => "c", "4" => "d", "5" => "e");
// While Schleife

$i = 0;
while ($i < 5) {
	echo $meinArray[$i];
	$i++;
}

// For Schleife

for ($i = 0; $i < 5; $i++) {
	echo $meinArray[$i];
}

// Foreach schleife
// Gibt jeden eintrag des Arrays einzeln zurück

foreach ($meinArray as $single) {
	echo $single;
}

?>