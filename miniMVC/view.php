<?php
	class View {
		public $_ = array();
		// Variable Zuweisen
		// $Name variablen name, $value der Inhalt
		public function assign($name,$value) {
			$this->_[$name] = $value;
		}
		public function getTemplate($name) {
			// erstmal uninteressant, Funktion braucht nicht geändert werden
			ob_start(NULL);
				// Header Datei soll immer geladen werden!
				include "template/header.php";
				
				// Individuell ausgewählte Datei, kommt aus control.php
				include "template/$name.php";
				// Includierte Inhalte werden in Variable geschrieben, nicht direkt ausgegeben
				$content = ob_get_contents();
			ob_end_clean();
			
			return $content;
		}
	}
?>