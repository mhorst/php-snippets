<html>
<head>
<title>miniMVC</title>
</head>
<body>
<?php
	include 'control.php';
	// Post und Get Daten zusammenfassen
	$request = array_merge($_POST,$_GET);
	
	// Control starten und Post und Get Daten übergeben
	$control = new Control($request);
	
	// "Programm" starten
	$control->main();
	
	// Inhalt ausgeben
	echo $control->content;
?>
</body>
</html>