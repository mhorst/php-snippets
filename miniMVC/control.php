<?php
	/*
	 * Kontroll Klasse:
	 * Hier wird alles gesteuert! Die Daten werden von der Index Datei über den Constructor in die Klasse gebracht (Z. 15)
	 * 
	 */
	include "view.php";
	include "model.php";
	class Control {
		private $model;
		private $view;
		public $request;
		public $content;
		
		public function __construct($request) {
			$this->request = $request;
			$this->model = new Model($request);
			$this->view = new View();
		}
		
		// Von der Index.php wird die main Funktion aufgerufen und das "Programm gestartet"
		public function main() {
			// "@" wird geseztt um keine Fehlermeldung zu erhalten wenn view nicht gesetzt ist
			// @ verhindert sämtliche fehlermeldungen!
			// $this->request['view'] ist die Seite die wir aufrufen wollen, im Browser sieht es so aus: index.php?view=NAME DES GEWÜNSCHTEN INHALTES
			switch(@$this->request['view']) {
				// index.php?view=about
				case 'about':
					// Einen Wert ins Template bekommen
					// Die Variable kann in der Template Datei benutzt werden $this->_['timestamp(Oder anderer Name wenn gestzt)']
					// time() erzeugt einen unix Timestamp
					$this->view->assign("timestamp", time());
					// In der View Klasse wird das Template geholt, getTemplate($name), wobei die Datei im Tempaltes Ordner liegen muss und $name.php heißen muss
					$this->content = $this->view->getTemplate("about");
				break;
				// index.php?view=start oder // index.php
				case 'start':
				case 'home':
				default:
					$this->content = $this->view->getTemplate("start");
				break;
			}
		}
	}
?>