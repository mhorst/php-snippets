<html>
	<head>
		<title>Ajax</title>
	</head>
	<!-- jQuery einbinden -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>
		// Warten bis das Dokument fertig geladen hat
		$(document).ready(function(){
			// Bei jedem Tastendruck bzw. danach in dem Feld wird folgendes ausgeführt
			$(".username").on("keyup", function(){
				// Ajax request auf ajax.php, die Ausgabe in der ajax.php wird in den div mit der Klasse result geschrieben
				$.ajax({
					url: "ajax.php",
					type: "POST",
					data: $(".ajax").serialize(),
					success: function(data){
						$(".result").html(data);
					}
				});
			});
		});	
	</script>
	<body>
		<form name="ajax" class="ajax">
			<input type="text" name="username" class="username" />
		</form>
		<div class="result" style="padding: 10px; border: 1px solid;">
			
		</div>
	</body>
</html>