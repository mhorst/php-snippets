<?php

class meineKlasse {
	private $geheim = "Psst";
	public $oeffentlich = "Blabla";

	public function __construct($request = false) {
		echo "Willkommen in der Klasse";
	}

	public function say($msg = "") {
		echo $msg;
	}

	public function addiere($zahl1, $zahl2) {
		return $zahl1 + $zahl2;
	}

}

$dieKlassenVariable = new meineKlasse();
$dieKlassenVariable -> say("Hallo Welt");
// ==> Hallo Welt

echo $dieKlassenVariable -> addiere(5, 10);
// ==> 15
?>